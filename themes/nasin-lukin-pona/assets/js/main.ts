interface HTMLElement {
    scrollTo(y: number, x: number): void
}

HTMLElement.prototype.scrollTo = function(y: number, x: number = 0): void {
    this.scrollTop = y
    this.scrollLeft = x
}

document.querySelector(".nlp-scrim").addEventListener("scroll", function (e) {
    let targ = e.target as HTMLElement
    if (targ.scrollTop == 0) {
        CloseDrawer()
    }
})

let items = new Array<HTMLElement>();

document.querySelectorAll("#TableOfContents a").forEach(elm => {
    let id = elm.getAttribute("href")
    items.push(document.querySelector(id))
    items.sort((a, b) => {
        if (a.offsetTop < b.offsetTop) {
            return -1
        } else if (a.offsetTop > b.offsetTop) {
            return 1
        }
        return 0
    })
})

window.addEventListener("scroll", function(e) {
    const currentIdx = items.findIndex(sec => sec.offsetTop >= window.scrollY)
    document.querySelector(".nlp-active")?.classList?.remove("nlp-active")
    document.querySelector(`a[href="#${items[currentIdx].id}"]`).classList.add("nlp-active")
})

const flippedClass = "nlp-flipcard--flipped";

let cards = document.querySelectorAll(".nlp-flipcard")
cards.forEach(element => {
    element.addEventListener("click", function(e) {
        if (element.classList.contains(flippedClass)) {
            element.classList.remove(flippedClass)
        } else {
            element.classList.add(flippedClass)
        }
    })
});

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function OpenDrawer() {
    let drawer = document.querySelector(".nlp-paned__left")
    let scrim = document.querySelector(".nlp-scrim")
    scrim.appendChild(drawer)
    scrim.classList.add("nlp-scrim--open")
    
    setTimeout(() => {
        drawer.classList.add("nlp-paned__left--reparented")
        drawer.classList.add("nlp-paned__left--open")
    }, 150)

    let padding = document.querySelector(".nlp-scrim__padder")

    if (drawer.scrollHeight < window.innerHeight) {
        scrim.scrollTo(padding.scrollHeight + drawer.scrollHeight, 0)
    } else {
        scrim.scrollTo(padding.scrollHeight / 2, 0)
    }
}
function CloseDrawer() {
    let drawer = document.querySelector(".nlp-paned__left")
    drawer.classList.remove("nlp-paned__left--open")
    drawer.classList.remove("nlp-paned__left--reparented")
    let scrim = document.querySelector(".nlp-scrim")
    scrim.classList.remove("nlp-scrim--open")
    document.querySelector(".nlp-paned").prepend(drawer)
}