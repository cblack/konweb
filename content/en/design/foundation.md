---
title: "Foundation"
categories: ["Design"]
---

# Units

The base Kon unit is a device-independent pixel which can be referred to as `dp` or `epx` for short.

# Grids

The sizes, margins, paddings, and positions of UI elements in your Kon apps should be aligned to multiples of 4 units.

Kon apps adapt to a wide variety of device scales, and 4 units remains integer at multiples of 25% which covers most common scaling situations (100%, 125%, 150%, 175%, 200%, 225%, and so on.)
Using multiples of four allows your UI elements to remain crisp across scales.

# Materials

Kon applications are built out of two materials: solid Paper and transparent Plasma.

![Left: Paper](/control.png)

## Specifications

### Plasma

Plasma is 80% transparent with a 16dp blur which is ideally Gaussian.

# Controls

Kon Controls are composed of a few key elements:

![A control's anatomy.](/materials.png)

## Background

The background decoration of a control behind the Content.

## Content

The main content of the control, such as a button's text and its icon, or a color well's current colour.

## Padding

The distance between the control's Content and the edge of its Background.

## Border

An inner stroke on the Background.

## Shadow

A shadow behind the Background.